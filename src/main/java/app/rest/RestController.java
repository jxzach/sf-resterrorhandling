package app.rest;

import app.core.annotation.ResourceNotFound;
import app.dto.Data;
import app.dto.Request;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.InetAddress;
import java.util.Date;


@org.springframework.web.bind.annotation.RestController
@Slf4j
@RequestMapping("/rest")
public class RestController {

    @GetMapping("/time")
    public String getTime() {
        try {
            InetAddress ia = InetAddress.getLocalHost();
            return ia + " - " + new Date();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @ResourceNotFound
    @GetMapping("/{id}")
    public Data getById(@PathVariable(name = "id") Long id) {
        if (id == 0L)
            return null;
        if (id < 0)
            throw new RuntimeException("Less than 0");

        Data data = new Data();
        data.setId(id);
        return data;
    }

    @PostMapping("/{id}")
    public Data post(@PathVariable(name = "id") Long id, @RequestBody @Valid Request request) throws Exception {
        Thread.sleep(100L);
        return getById(id);
    }
}
