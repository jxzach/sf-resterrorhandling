package app.core;

import app.core.exception.RequestNotValidException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * @Valid validation - 422
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String msg = "Parameter: " + ex.getParameter().getParameter().getName();
        if (!ex.getBindingResult().getFieldErrors().isEmpty()) {
            msg += "; fields=[" + ex.getBindingResult().getFieldErrors().stream().map(fe -> fe.getField() + "=" + fe.getRejectedValue()).collect(Collectors.joining(", ")) + "]";
        }
        if (!ex.getBindingResult().getGlobalErrors().isEmpty()) {
            msg += "; errors=[" + ex.getBindingResult().getGlobalErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.joining(", ")) + "]";
        }

        ResponseMessage ed = new ResponseMessage(ErrorCode.REQUEST_VALIDATION, msg);
        MDC.put(MDCContext.INCIDENT_ID.getCode(), ed.getIncidentId());
        return ResponseEntity.status(ed.getStatus()).body(ed);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        return super.handleHttpRequestMethodNotSupported(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String msg = "Unexpected request content type: " + ex.getMessage();

        ResponseMessage ed = new ResponseMessage(ErrorCode.UNSUPPORTED_MEDIA_TYPE, msg);
        MDC.put(MDCContext.INCIDENT_ID.getCode(), ed.getIncidentId());
        log.error("Unexpected media type", ex);
        return ResponseEntity.status(ed.getStatus()).body(ed);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        return super.handleHttpMediaTypeNotAcceptable(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        return super.handleMissingPathVariable(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String msg = "Missing required input parameter: " + ex.getParameterName() + "@" + ex.getParameterType();

        ResponseMessage ed = new ResponseMessage(ErrorCode.REQUEST_VALIDATION, msg);
        MDC.put(MDCContext.INCIDENT_ID.getCode(), ed.getIncidentId());
        return ResponseEntity.status(ed.getStatus()).body(ed);
    }

    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        return super.handleServletRequestBindingException(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleConversionNotSupported(ConversionNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        return super.handleConversionNotSupported(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        // 400 - input param conversopm
        return super.handleTypeMismatch(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String msg = "Unparsable or unreadable message: " + ex.getMessage();
        ResponseMessage ed = new ResponseMessage(ErrorCode.REQUEST_VALIDATION, msg);
        MDC.put(MDCContext.INCIDENT_ID.getCode(), ed.getIncidentId());
        return ResponseEntity.status(ed.getStatus()).body(ed);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        return super.handleHttpMessageNotWritable(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        return super.handleMissingServletRequestPart(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        return super.handleBindException(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.warn("Unhandled exception", ex);
        return super.handleNoHandlerFoundException(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {
        log.warn("Unhandled exception", ex);
        return super.handleAsyncRequestTimeoutException(ex, headers, status, webRequest);
    }

    /**
     * 400, 422 - all validations
     */
    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (ex instanceof MethodArgumentTypeMismatchException) {
            MethodArgumentTypeMismatchException e = (MethodArgumentTypeMismatchException)ex;

            String msg = "Value conversion: param=" + e.getName() + ", error=" + ex.getMessage();

            ResponseMessage ed = new ResponseMessage(ErrorCode.REQUEST_VALIDATION, msg);
            MDC.put(MDCContext.INCIDENT_ID.getCode(), ed.getIncidentId());
            return ResponseEntity.status(ed.getStatus()).body(ed);
        }

        return super.handleExceptionInternal(ex, body, headers, status, request);

    }

    @org.springframework.web.bind.annotation.ExceptionHandler({ app.core.exception.ResourceNotFoundException.class })
    protected ResponseEntity<Object> handleResourceNotFound(Exception ex, WebRequest request) {
        ResponseMessage ed = new ResponseMessage(ErrorCode.ENTITY_NOT_FOUND);
        return ResponseEntity.status(ed.getStatus()).body(ed);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler({ app.core.exception.RequestNotValidException.class })
    protected ResponseEntity<Object> handleRequestNotValid(RequestNotValidException ex, WebRequest request) {
        String msg = "Request validation error: " + ex.getMessage() + " [ " + ex.validationsToString(", ") + " ]";

        ResponseMessage ed = new ResponseMessage(ErrorCode.REQUEST_VALIDATION, msg);
        MDC.put(MDCContext.INCIDENT_ID.getCode(), ed.getIncidentId());
        log.error(msg, ex);
        return ResponseEntity.status(ed.getStatus()).body(ed);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler({ ResponseStatusException.class })
    protected ResponseEntity<Object> handleResponseStatusException(ResponseStatusException ex, WebRequest request) {
        String msg = ex.getReason();

        ResponseMessage ed = new ResponseMessage(ErrorCode.valueOf(ex.getStatus()), msg);
        MDC.put(MDCContext.INCIDENT_ID.getCode(), ed.getIncidentId());
        log.error(msg, ex);
        return ResponseEntity.status(ed.getStatus()).body(ed);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler
    protected ResponseEntity<Object> handleUnhandledException(Exception ex, WebRequest request) {
        String msg = "Application error: " + (ex.getMessage() != null? ex.getMessage(): ex.getClass().getName());

        ResponseMessage ed = new ResponseMessage(ErrorCode.APPLICATION_ERROR, msg);
        MDC.put(MDCContext.INCIDENT_ID.getCode(), ed.getIncidentId());
        log.error("Application unhandled error", ex);
        return ResponseEntity.status(ed.getStatus()).body(ed);
    }
}
