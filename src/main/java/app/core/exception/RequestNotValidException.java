package app.core.exception;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Request not valid")
public class RequestNotValidException extends RuntimeException {

	private final List<Validation> validations = new ArrayList<>();

	public RequestNotValidException(final String message) {
		super(message);
	}

	public RequestNotValidException(final String message, Validation... validations) {
		super(message);
		if (validations != null) {
			this.validations.addAll(Arrays.asList(validations));
		}
	}

	public RequestNotValidException(Validation... validations) {
		this("Request not valid", validations);
	}

	public List<Validation> getValidations() {
		return validations;
	}

	public String validationsToString(String delimiter) {
		return validations.stream().map(Validation::toString).collect(Collectors.joining(delimiter));
	}

	public static class Validation {
		public String validation;
		public String error;

		public static Validation create(final String validation, final String validationError) {
			Preconditions.checkArgument(StringUtils.isNotBlank(validation), "Validation is blank");
			Preconditions.checkArgument(StringUtils.isNotBlank(validationError), "Validation error is blank");
			return new Validation(validation, validationError);
		}

		private Validation(final String validation, final String error) {
			this.validation = validation;
			this.error = error;
		}

		public String getValidation() {
			return validation;
		}

		public String getError() {
			return error;
		}

		@Override
		public String toString() {
			return validation + ": " + error;
		}
	}
}
