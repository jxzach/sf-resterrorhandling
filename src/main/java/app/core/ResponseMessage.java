package app.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.Preconditions;

import java.util.UUID;

/**
 * Universal HTTP Error Response
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResponseMessage {

    private String incidentId;
    private ErrorCode error;
    private String detail;
    private Throwable throwable;

    public ResponseMessage(ErrorCode error) {
        Preconditions.checkNotNull(error, "ErrorCode is null");
        this.error = error;
        if (isError())
            incidentId = UUID.randomUUID().toString();
    }

    public ResponseMessage(ErrorCode error, String detail) {
        this(error);
        this.detail = detail;
    }

    public ResponseMessage(ErrorCode error, String detail, Throwable t) {
        this(error, detail);
        this.throwable = t;
    }

    public String getIncidentId() {
        return incidentId;
    }

    public Integer getErrorCode() {
        return error.getCode();
    }

    public int getStatus() {
        return error.getStatus().value();
    }

    public String getMessage() {
        return error.getMessage();
    }

    public String getDetail() {
        return detail;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public String getMessageKey() {
        return error.getMessageKey();
    }

    private boolean isError() {
        return error.getCode() != null && error.getCode() != 0;
    }
}
