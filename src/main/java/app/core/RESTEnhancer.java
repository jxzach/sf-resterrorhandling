package app.core;

import app.core.annotation.ResourceNotFound;
import app.core.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;

@Slf4j
@Configuration
@Aspect
public class RESTEnhancer {

    @Around("within(@org.springframework.web.bind.annotation.RestController *)")
    public Object resultCheck(ProceedingJoinPoint call) throws Throwable {
        MethodSignature signature = (MethodSignature) call.getSignature();
        Method method = signature.getMethod();

        Object result = null;
        result = call.proceed();

        if (result == null) {
            ResourceNotFound annnotation = method.getAnnotation(ResourceNotFound.class);
            if (annnotation != null) {
                throw new ResourceNotFoundException(annnotation.message());
            }
        }

        return result;
    }
}
