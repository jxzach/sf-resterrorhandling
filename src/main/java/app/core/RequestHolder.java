package app.core;

import lombok.Getter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component()
@RequestScope
@Getter
public class RequestHolder {

    private final Long startTime = System.currentTimeMillis();

    public void init() {
        /* empty */
    }

}
