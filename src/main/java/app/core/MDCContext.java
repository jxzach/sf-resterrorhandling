package app.core;

import lombok.Getter;

@Getter
public enum MDCContext {

    PRINCIPAL("princ"),
    REQUEST_ID("rid"),
    INCIDENT_ID("iid"),
    PROCESSING_TIME_IN_MS("pms"),

    ORDER_ID("oid"),
    CUSTOMER_ID("cid")
    ;

    /** Used in MDC context, etc. */
    private String code;

    private MDCContext(String code) {
        this.code = code;
    }
}
