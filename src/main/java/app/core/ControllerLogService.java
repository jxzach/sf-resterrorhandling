package app.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HeaderElement;
import org.apache.http.message.BasicHeaderValueParser;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

@Slf4j
@Component
public class ControllerLogService {

    private final ObjectMapper mapper;

    public ControllerLogService(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public void logRequest(HttpServletRequest request, Object body) {
        try {
            String msg = request.getMethod();
            msg += " " + request.getRequestURI();

            if (request.getQueryString() != null)
                msg += "?" + request.getQueryString();

            String contentType = getContentType(request.getContentType());
            if (isJsonContentType(contentType)) {
                if (body != null) {
                    msg += " " + mapper.writeValueAsString(body);
                }
            } else if (contentType != null) {
                msg += "; contentType=" + contentType;
            }

            log.debug("> " + msg);
        } catch (Exception e) {
            log.error("Controller request logging failed", e);
        }
    }

    public void logResponse(HttpServletRequest request, HttpServletResponse response, MediaType responseMediaType, Object body) {
        try {
            String msg = "< " + response.getStatus();

            String contentType = responseMediaType != null? responseMediaType.toString():  null;
            if (isJsonContentType(contentType)) {
                if (body != null) {
                    msg += " " + mapper.writeValueAsString(body);
                }
            } else {
                msg += "; contentType=" + contentType;
            }

            if (response.getStatus() >= 500) {
                log.info("REQ Headers:\n" + headersToString(request));
                log.error(msg);
            } else if (response.getStatus() >= 400) {
                log.warn(msg);
            } else {
                log.debug(msg);
            }
        } catch (Exception e) {
            log.error("Controller response logging failed", e);
        }
    }

    private String headersToString(HttpServletRequest request) {
        StringBuilder str = new StringBuilder();
        Enumeration<String> en = request.getHeaderNames();
        while(en.hasMoreElements()) {
            String header = en.nextElement();
            if (str.length() > 0)
                str.append("\n");
            str.append(header).append("=").append(request.getHeader(header));
        }
        return str.toString();
    }

    private String getContentType(String headerContentType) {
        if (StringUtils.isBlank(headerContentType))
            return null;
        HeaderElement headerElement = BasicHeaderValueParser.parseHeaderElement(headerContentType, null);
        return headerElement.getName();
    }

    private boolean isJsonContentType(String contentType) {
        return MimeTypeUtils.APPLICATION_JSON.toString().equalsIgnoreCase(contentType);
    }
}
