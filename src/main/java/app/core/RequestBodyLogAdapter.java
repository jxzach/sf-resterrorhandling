package app.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;

@RestControllerAdvice
public class RequestBodyLogAdapter extends RequestBodyAdviceAdapter {

    private final ControllerLogService logService;
    private final HttpServletRequest request;
    private final RequestHolder requestHolder;

    @Autowired
    public RequestBodyLogAdapter(
            ControllerLogService logService,
            HttpServletRequest request,
            RequestHolder requestHolder
    ) {
        this.logService = logService;
        this.request = request;
        this.requestHolder = requestHolder;
    }


    @Override
    public boolean supports(MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        requestHolder.init();
        logService.logRequest(request, body);
        return super.afterBodyRead(body, inputMessage, parameter, targetType, converterType);
    }
}
