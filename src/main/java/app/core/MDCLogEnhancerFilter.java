package app.core;

import lombok.extern.log4j.Log4j2;
import org.slf4j.MDC;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.UUID;

@Log4j2
@Component
public class MDCLogEnhancerFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        // TODO: pricipal, order id,...
        MDC.put(MDCContext.REQUEST_ID.getCode(), UUID.randomUUID().toString());
        addPrincipal();
        try {
            filterChain.doFilter(request, response);
        } finally {
            MDC.clear();
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void destroy() {
    }

    private void addPrincipal() {
        try {
            SecurityContext sc = SecurityContextHolder.getContext();
            if (sc != null && sc.getAuthentication() != null && sc.getAuthentication().getPrincipal() != null) {
                MDC.put(MDCContext.PRINCIPAL.getCode(),"\"" + ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername() + "\"");
            }
        } catch (Exception e) {
            // log.warn("MDC setting error: " + e.getMessage());
        }
    }
}
