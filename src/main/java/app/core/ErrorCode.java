package app.core;

import com.google.common.base.Preconditions;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

@Getter
public enum ErrorCode {

    ENTITY_NOT_FOUND(null, HttpStatus.NOT_FOUND, "Entity not found", null),

    REQUEST_VALIDATION(100, HttpStatus.UNPROCESSABLE_ENTITY, "Invalid request", null),

    UNSUPPORTED_MEDIA_TYPE(110, HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Unexpected request media type", null),

    APPLICATION_ERROR(1000, HttpStatus.INTERNAL_SERVER_ERROR, "Application error", null)
    ;

    public static ErrorCode valueOf(HttpStatus status) {
        Preconditions.checkNotNull(status, "HttpStatus is null");
        return Arrays.stream(values()).filter(c -> c.getStatus().equals(status)).findFirst().orElse(APPLICATION_ERROR);
    }

    private ErrorCode(Integer code, HttpStatus status, String message, String messageKey) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.messageKey = messageKey;
    }

    private Integer code;
    private HttpStatus status;
    private String message;
    private String messageKey;
}
