package app.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.servlet.handler.MappedInterceptor;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass=true) 
public class FrameworkConfiguration {

    private final LogInterceptor logInterceptor;

    @Autowired
    public FrameworkConfiguration(LogInterceptor logInterceptor) {
        this.logInterceptor = logInterceptor;
    }

    @Bean
    public MappedInterceptor registerLogInterceptor() {
        return new MappedInterceptor(
                null,  // => maps to any repository
                logInterceptor
        );
    }


}
