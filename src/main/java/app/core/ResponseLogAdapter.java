package app.core;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@SuppressWarnings("rawtypes")
@RestControllerAdvice
public class ResponseLogAdapter implements ResponseBodyAdvice {

    private final ControllerLogService logService;
    private final RequestHolder requestHolder;

    @Autowired
    public ResponseLogAdapter(
            ControllerLogService logService,
            RequestHolder requestHolder
    ) {
        this.logService = logService;
        this.requestHolder = requestHolder;
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        if (serverHttpRequest instanceof ServletServerHttpRequest && serverHttpResponse instanceof ServletServerHttpResponse) {
            MDC.put(MDCContext.PROCESSING_TIME_IN_MS.getCode(), Long.toString(System.currentTimeMillis() - requestHolder.getStartTime()));
            logService.logResponse(((ServletServerHttpRequest) serverHttpRequest).getServletRequest(), ((ServletServerHttpResponse) serverHttpResponse).getServletResponse(), mediaType, o);
        }

        return o;
    }
}
