package app.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * GET logger
 */
@Component
public class LogInterceptor implements HandlerInterceptor {

    private final ControllerLogService logService;
    private final RequestHolder requestHolder;

    @Autowired
    public LogInterceptor(
            ControllerLogService logService,
            RequestHolder requestHolder
    ) {
        this.logService = logService;
        this.requestHolder = requestHolder;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (DispatcherType.REQUEST.name().equals(request.getDispatcherType().name()) && request.getMethod().equals(HttpMethod.GET.name())) {
            requestHolder.init();
            logService.logRequest(request, null);
        }
        return true;
    }
}
