package com.blueplustechnologies.heartbeat.core.validation;

import app.core.validation.RequestProperty;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequestValidation {

	RequestProperty[] value();
	
}
