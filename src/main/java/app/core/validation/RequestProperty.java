package app.core.validation;

import org.springframework.web.bind.MethodArgumentNotValidException;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestProperty {

	String name();
	boolean checkNotNull() default false;
	boolean checkNull() default false;
	Class<? extends Throwable> exception() default MethodArgumentNotValidException.class;
	String overwrite() default "";

}
